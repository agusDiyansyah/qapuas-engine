<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Page extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS page;
		");
		$this->db->query("
			CREATE TABLE `page` (
				`id_page` int(11) NOT NULL AUTO_INCREMENT,
				`title` varchar(255) DEFAULT NULL,
				`slug` varchar(255) DEFAULT NULL,
				`views` int(11) DEFAULT 0,
				`content` text DEFAULT NULL,
				PRIMARY KEY (`id_page`) USING BTREE
			) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
		");
	}

	public function down () {
	}
}