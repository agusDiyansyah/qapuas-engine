<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Revisi_berita extends CI_Migration {
	public function up () {
		$this->db->query("
			ALTER TABLE berita
			ADD sinopsis TEXT;
		");
		$this->db->query("
			ALTER TABLE berita
			ADD id_admin INT(11);
		");
	}

	public function down () {
		// $this->db->query("
		// 	DROP TABLE admin_level;
		// ");
	}
}