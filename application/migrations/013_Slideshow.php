<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Slideshow extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS slideshow;
		");
		$this->db->query("
			CREATE TABLE `slideshow` (
				`id_slideshow` int(11) NOT NULL AUTO_INCREMENT,
				`title` varchar(255) DEFAULT NULL,
				`slug` varchar(255) DEFAULT NULL,
				`sinopsis` text DEFAULT NULL,
				`posisi` enum('kanan', 'tengah', 'kiri') DEFAULT 'tengah',
				`content` text DEFAULT NULL,
				`file` text DEFAULT NULL,
				PRIMARY KEY (`id_slideshow`) USING BTREE
			) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;
		");
	}

	public function down () {
	}
}