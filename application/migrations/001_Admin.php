<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Admin extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS admin;
		");
		$this->db->query("
			CREATE TABLE `admin` (
				`id_admin` int(20) unsigned NOT NULL AUTO_INCREMENT,
				`pass` varchar(64) NOT NULL,
				`username` varchar(100) NOT NULL,
				`nama_lengkap` varchar(200) DEFAULT '',
				`email` varchar(200) DEFAULT '',
				`avatar` text NOT NULL,
				`banned` tinyint(1) DEFAULT 0,
				`id_level` int(11) DEFAULT NULL,
				`last_login` datetime DEFAULT NULL,
				`last_activity` datetime DEFAULT NULL,
				`date_created` datetime DEFAULT current_timestamp(),
				`forgot_exp` text DEFAULT NULL,
				`remember_time` datetime DEFAULT NULL,
				`remember_exp` text DEFAULT NULL,
				`verification_code` text DEFAULT NULL,
				`top_secret` varchar(16) DEFAULT NULL,
				`ip_address` text DEFAULT NULL,
				PRIMARY KEY (`id_admin`)
			);
		");
		$this->db->insert_batch("admin", array(
			array(
				"pass" => '$2y$12$SfeXqe00ksE5bweKLLL.QO67v1vOzKu8cTXg3qiXk02TbpbmNd5qi',
				"username" => "admin",
				"nama_lengkap" => "AGUS DIYANSYAH",
				"email" => "agusdiyansyah@gmail.com",
				"avatar" => "8f17181c6038d76b451058827f27d3c0.jpg",
				"banned" => "0",
				"id_level" => "1",
				"last_login" => "2018-10-17 20:35:32",
				"last_activity" => "",
				"date_created" => "2018-05-14 15:14:05",
				"forgot_exp" => "",
				"remember_time" => "",
				"remember_exp" => "",
				"verification_code" => "BF3FE0C",
				"top_secret" => "",
				"ip_address" => "127.0.0.1",
			),
			array(
				"pass" => '$2y$12$SfeXqe00ksE5bweKLLL.QO67v1vOzKu8cTXg3qiXk02TbpbmNd5qi',
				"username" => "diyan",
				"nama_lengkap" => "AGUS DIYANSYAH",
				"email" => "diyan@mail.com",
				"avatar" => "8f17181c6038d76b451058827f27d3c0.jpg",
				"banned" => "0",
				"id_level" => "1",
				"last_login" => "2018-10-17 20:35:32",
				"last_activity" => "",
				"date_created" => "2018-05-14 15:14:05",
				"forgot_exp" => "",
				"remember_time" => "",
				"remember_exp" => "",
				"verification_code" => "BF3FE0C",
				"top_secret" => "",
				"ip_address" => "127.0.0.1",
			),
			array(
				"pass" => '$2y$12$SfeXqe00ksE5bweKLLL.QO67v1vOzKu8cTXg3qiXk02TbpbmNd5qi',
				"username" => "rio",
				"nama_lengkap" => "RIO GUNAWAN",
				"email" => "rio@mail.com",
				"avatar" => "8f17181c6038d76b451058827f27d3c0.jpg",
				"banned" => "0",
				"id_level" => "2",
				"last_login" => "2018-10-17 20:35:32",
				"last_activity" => "",
				"date_created" => "2018-05-14 15:14:05",
				"forgot_exp" => "",
				"remember_time" => "",
				"remember_exp" => "",
				"verification_code" => "BF3FE0C",
				"top_secret" => "",
				"ip_address" => "127.0.0.1",
			),
		));
	}

	public function down () {}
	
}