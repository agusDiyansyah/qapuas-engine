<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Admin_level extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS admin_level;
		");
		$this->db->query("
			CREATE TABLE `admin_level` (
				`id_level` int(11) NOT NULL AUTO_INCREMENT,
				`level` varchar(100) DEFAULT NULL,
				PRIMARY KEY (`id_level`) USING BTREE
			);
		");
		$this->db->insert_batch("admin_level", array(
			array(
				'id_level' => '1',
				'level' => 'ADMIN'
			),
			array(
				'id_level' => '2',
				'level' => 'REDAKTUR'
			),
		));
	}

	public function down () {
		// $this->db->query("
		// 	DROP TABLE admin_level;
		// ");
	}
}