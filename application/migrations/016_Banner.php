<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Banner extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS banner;
		");
		$this->db->query("
			CREATE TABLE `banner` (
				`id_banner` int(11) NOT NULL AUTO_INCREMENT,
				`title` varchar(255) DEFAULT NULL,
				`status` tinyint(4) DEFAULT NULL,
				`file` varchar(255) DEFAULT NULL,
				PRIMARY KEY (`id_banner`) USING BTREE
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
		");
	}

	public function down () {
	}
}