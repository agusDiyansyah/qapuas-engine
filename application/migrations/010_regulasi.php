<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_regulasi extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS regulasi;
		");
		$this->db->query("
			CREATE TABLE `regulasi` (
				`id_regulasi` int(11) NOT NULL AUTO_INCREMENT,
				`regulasi` varchar(255) DEFAULT NULL,
				`slug` varchar(255) DEFAULT NULL,
				`file` varchar(255) DEFAULT NULL,
				`tanggal` date DEFAULT NULL,
				PRIMARY KEY (`id_regulasi`) USING BTREE
			) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;
		");
	}

	public function down () {
	}
}