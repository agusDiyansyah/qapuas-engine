<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_agenda extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS agenda;
		");
		$this->db->query("
			CREATE TABLE `agenda` (
				`id_agenda` int(11) NOT NULL AUTO_INCREMENT,
				`agenda` varchar(255) DEFAULT NULL,
				`tanggal` date DEFAULT NULL,
				`jam_mulai` time DEFAULT NULL,
				`jam_selesai` time DEFAULT NULL,
				`tempat` varchar(255) DEFAULT NULL,
				PRIMARY KEY (`id_agenda`) USING BTREE
			) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;
		");
	}

	public function down () {
	}
}