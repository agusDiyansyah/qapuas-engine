<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Revisi_berita extends CI_Migration {
	public function up () {
		$this->db->query("
			ALTER TABLE berita
			DROP id_berita_kategori;
		");
		
		$this->db->query("
			ALTER TABLE berita
			ADD status ENUM('publish', 'draft') DEFAULT 'draft';
		");
	}

	public function down () {
		// $this->db->query("
		// 	DROP TABLE admin_level;
		// ");
	}
}