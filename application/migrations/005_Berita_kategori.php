<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Berita_kategori extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS berita_kategori;
		");
		$this->db->query("
			CREATE TABLE `berita_kategori` (
				`id_berita_kategori` int(11) NOT NULL AUTO_INCREMENT,
				`berita_kategori` varchar(255) DEFAULT NULL,
				`slug` varchar(255) DEFAULT NULL,
				PRIMARY KEY (`id_berita_kategori`) USING BTREE
			) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
		");
	}

	public function down () {
	}
}