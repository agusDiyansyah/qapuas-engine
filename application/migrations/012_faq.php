<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Faq extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS faq;
		");
		$this->db->query("
			CREATE TABLE `faq` (
				`id_faq` int(11) NOT NULL AUTO_INCREMENT,
				`id_faq_kategori` int(11) DEFAULT 0,
				`pertanyaan` varchar(255) DEFAULT NULL,
				`jawaban` text DEFAULT NULL,
				PRIMARY KEY (`id_faq`) USING BTREE
			) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;
		");
	}

	public function down () {
	}
}