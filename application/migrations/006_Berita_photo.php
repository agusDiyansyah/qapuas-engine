<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Berita_photo extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS berita_photo;
		");
		$this->db->query("
			CREATE TABLE `berita_photo` (
				`id_berita_photo` int(11) NOT NULL AUTO_INCREMENT,
				`id_berita` int(11) DEFAULT NULL,
				`file` varchar(255) DEFAULT NULL,
				`order` tinyint(2) DEFAULT 0,
				PRIMARY KEY (`id_berita_photo`) USING BTREE
			) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;
		");
	}

	public function down () {
	}
}