<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Faq_kategori extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS faq_kategori;
		");
		$this->db->query("
			CREATE TABLE `faq_kategori` (
				`id_faq_kategori` int(11) NOT NULL AUTO_INCREMENT,
				`faq_kategori` varchar(255) DEFAULT NULL,
				PRIMARY KEY (`id_faq_kategori`) USING BTREE
			) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;
		");
	}

	public function down () {
	}
}