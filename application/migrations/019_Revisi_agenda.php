<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Revisi_agenda extends CI_Migration {
	public function up () {
		$this->db->query("
			ALTER TABLE agenda
			DROP tanggal
		");
		
		$this->db->query("
			ALTER TABLE agenda
			ADD slug varchar(255);
		");
		$this->db->query("
			ALTER TABLE agenda
			ADD tanggal_mulai date;
		");
		$this->db->query("
			ALTER TABLE agenda
			ADD tanggal_selesai date;
		");
		$this->db->query("
			ALTER TABLE agenda
			ADD file varchar(255);
		");
		$this->db->query("
			ALTER TABLE agenda
			ADD keterangan text;
		");
	}

	public function down () {}
}