<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Berita extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS berita;
		");
		$this->db->query("
			CREATE TABLE `berita` (
				`id_berita` int(11) NOT NULL AUTO_INCREMENT,
				`id_berita_kategori` int(11) DEFAULT NULL,
				`slug` varchar(255) DEFAULT NULL,
				`title` varchar(255) DEFAULT NULL,
				`content` text DEFAULT NULL,
				`views` int(11) DEFAULT 0,
				`date_create` timestamp NOT NULL DEFAULT current_timestamp(),
				`date_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
				PRIMARY KEY (`id_berita`) USING BTREE
			) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
		");
	}

	public function down () {
	}
}