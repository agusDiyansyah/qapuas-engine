<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Kategori_regulasi extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS regulasi_kategori;
		");
		$this->db->query("
			CREATE TABLE `regulasi_kategori` (
				`id_kategori` int(11) NOT NULL AUTO_INCREMENT,
				`kategori` varchar(255) DEFAULT NULL,
				`slug` varchar(255) DEFAULT NULL,
				PRIMARY KEY (`id_kategori`) USING BTREE
			) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;
		");
	}

	public function down () {
	}
}