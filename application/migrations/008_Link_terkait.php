<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Link_terkait extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS link_terkait;
		");
		$this->db->query("
			CREATE TABLE `link_terkait` (
				`id_link_terkait` int(11) NOT NULL AUTO_INCREMENT,
				`link_terkait` varchar(200) DEFAULT NULL,
				`sinopsis` text,
				`link` varchar(255) DEFAULT NULL,
				`file` varchar(255) DEFAULT NULL,
				PRIMARY KEY (`id_link_terkait`) USING BTREE
			) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;
		");
	}

	public function down () {
	}
}