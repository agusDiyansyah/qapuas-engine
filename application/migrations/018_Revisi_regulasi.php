<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Revisi_regulasi extends CI_Migration {
	public function up () {
		$this->db->query("
			ALTER TABLE regulasi
			ADD id_kategori INT(11);
		");
	}

	public function down () {}
}