<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Helpdesk extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS helpdesk;
		");
		$this->db->query("
			CREATE TABLE `helpdesk` (
				`id_helpdesk` int(11) NOT NULL AUTO_INCREMENT,
				`nama` varchar(255) DEFAULT NULL,
				`nik` varchar(255) DEFAULT NULL,
				`email` varchar(255) DEFAULT NULL,
				`hp` varchar(255) DEFAULT NULL,
				`pertanyaan` text DEFAULT NULL,
				`file` varchar(255) DEFAULT NULL,
				`date_create` timestamp DEFAULT current_timestamp(),
				PRIMARY KEY (`id_helpdesk`) USING BTREE
			) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;
		");
	}

	public function down () {
	}
}