<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(FCPATH . "application/modules/login/controllers/Login.php");

class Admin_Controller extends MX_Controller {
	
	protected $auth;

	public function __construct () {
		parent::__construct ();
		$this->auth = new Login();
		
		$this->auth->cek_login();
		
		$this->output->set_title("Administrator");
		$this->output->set_template("admin/default");
		
		$this->output->set_output_data("menu", $this->admin_menu());
	}
	
	private function admin_menu () {
		$list_master_data = array();
		
		array_push($list_master_data, array(
			"class" => "mn-klasifikasi",
			"title" => "Klasifikasi",
			"link" => base_url("klasifikasi/admin"),
		));

		array_push($list_master_data, array(
			"class" => "mn-mahasiswa",
			"title" => "Mahasiswa",
			"link" => base_url("mahasiswa/admin"),
		));

		array_push($list_master_data, array(
			"class" => "mn-galeri",
			"title" => "Galeri",
			"link" => base_url("galeri/admin"),
		));

		array_push($list_master_data, array(
			"title" => "Test",
			"class" => "mn-test",
			"child" => [
				[
					"class" => "mn-child",
					"title" => "Child",
					"link" => base_url(),
				]
			],
		));
		
		$data = array(
			array(
				"name" => "Dashboard",
				"class" => "mn-dashboard",
				"link" => site_url("home/admin"),
			),
			array(
				"list" => $list_master_data
			),
		);

		// header('content-type: application/json');
		// echo json_encode($data); die;

		return $data;
	}
	
}