<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Public_Controller extends MX_Controller {

	public function __construct () {
		parent::__construct ();
		
		// $this->output->set_output_data("kategori", $this->db->get("produk_kategori"));
		// $this->output->set_output_data("page", $this->db
		// 	->where_not_in("id_page", array(1, 2))
		// 	->get("page"));
		$this->output->set_template("public/default");
		
		$sql = $this->db->get("regulasi_kategori");
		$data = array();
		foreach ($sql->result() as $val) {
			array_push($data, array(
				"slug" => $val->slug,
				"kategori" => $val->kategori,
			));
		}
		$this->output->set_output_data("regulasi_kategori", $data);

		define("BASE_SISB", "http://sisb.pontianakkota.go.id/");
		define("BASE_portal", "http://ekbang.pontianakkota.go.id/");
	}
	
}