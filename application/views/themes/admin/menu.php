<ul class="navbar-nav bg-gradient-white sidebar sidebar-dark accordion shadow mn" id="accordionSidebar">

	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-top justify-content-left" href="#">
		<div class="sidebar-brand-icon">
			<div class="img" style="
				background-image: url('<?= base_url('assets/themes/admin/img/logo-pemkot.png') ?>');
				background-size: cover;
				background-position: center;
				
				width: 50px;
				height: 45px;

				margin-right: 15px;
			"></div>
		</div>
		<div class="brand-title">
			<b class="d-block mb-1">SIMANTRA</b>
			<p style="font-size: .5rem !important; font-weight: normal" class="m-0">
				Sistem Informasi Manajemen Integrasi Informasi dan Pertukaran Data
			</p>
		</div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider my-0">

	<?php foreach ($menu as $menu): ?>
		<?php if (!empty($menu['name'])): ?>
			<?php if (!empty($menu['link'])): ?>
				<li class="nav-item <?php echo @$menu['class'] ?>">
					<a class="nav-link" href="<?php echo $menu['link'] ?>">
						<i class="fa mr-2 fa-th-large"></i>
						<span><?php echo $menu['name'] ?></span>
					</a>
				</li>
			<?php else: ?>
				<hr class="sidebar-divider">
				<div class="sidebar-heading"><?php echo $menu['name'] ?></div>
			<?php endif; ?>
		<?php endif; ?>
		
		<?php if (!empty($menu['list'])): ?>
			<?php $idCollapse = 0; ?>
			<?php foreach ($menu['list'] as $list): ?>
				<?php $idCollapse++; ?>
				<?php if (!empty($list['child'])): ?>

					<li class="nav-item <?php echo $list['class'] ?>">
						<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse-<?php echo $idCollapse ?>" aria-expanded="true" aria-controls="collapse-<?php echo $idCollapse ?>">
							<i class="fa mr-2 fa-th-large"></i>
							<span><?php echo $list['title'] ?></span>
						</a>
						<div id="collapse-<?php echo $idCollapse ?>" class="collapse" aria-labelledby="heading-<?php echo $idCollapse ?>" data-parent="#accordionSidebar">
							<div class="bg-white py-2 collapse-inner">
								<!-- <h6 class="collapse-header">Custom Components:</h6> -->
								<?php foreach ($list['child'] as $child): ?>
									<a class="collapse-item <?php echo $child['class'] ?>" href="<?php echo $child['link'] ?>">
										<?php echo $child['title'] ?>
									</a>
								<?php endforeach; ?>
							</div>
						</div>
					</li>
					
				<?php else: ?>
					<li class="nav-item <?php echo $list['class'] ?>">
						<a class="nav-link" href="<?php echo $list['link'] ?>">
							<i class="fa mr-2 fa-th-large"></i>
							<span><?php echo $list['title'] ?></span>
						</a>
					</li>
				<?php endif; ?>
				
			<?php endforeach; ?>
		<?php endif; ?>
		
	<?php endforeach; ?>

	<!-- <div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div> -->

</ul>