<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title><?php echo $title ?></title>

		<script src="<?php echo base_url("assets/themes/admin/vendors/jquery/jquery.min.js") ?>" charset="utf-8"></script>
		<script src="<?php echo base_url("assets/themes/admin/vendors/jquery-jspdf/jspdf.min.js") ?>" charset="utf-8"></script>
		<script src="<?php echo base_url("assets/themes/admin/vendors/jquery-jspdf/html2canvas.js") ?>" charset="utf-8"></script>

		<script type="text/javascript">
			$(document).ready(function() {
				$(".btn-cetak").on('click', function(event) {
					event.preventDefault();
					window.print();
				});
				
				$('.btn-export-pdf').on('click', function(event) {
					event.preventDefault();
					$('#container').pdf();
				});
				
				$('ol, ul').tablerize();
			});
			
			jQuery.fn.tablerize = function() {
				return this.each(function() {
					var table = $('<table>');
					var tbody = $('<tbody>');
					var parent = $(this)[0].localName;
					var type = $(this)[0].type;
					var i = 0;
					$(this).find('li').each(function(i) {
						var values = $(this).html().split('*');
						var tr = $('<tr>');
						i++;
						$.each(values, function(y) {
							if (parent == 'ul') {
								tr.append($('<td style="vertical-align: top; border: none; padding-right: 10px;">').html('- '));
							}
							if (parent == 'ol') {
								tr.append($('<td style="vertical-align: top; border: none; padding-right: 10px;">').html(i+' '));
							}
							tr.append($('<td style="border: none">').html(values[y]));
						});
						tbody.append(tr);
					});
					$(this).after(table.append(tbody)).remove();
				});
			};
			
			jQuery.fn.pdf = function () {
				html2canvas($(this), {
					onrendered: function (c) {
						document.body.appendChild(c);
						
						var img = c.toDataURL('image/png');
						var doc = new jsPDF();
						var title = $('.title').data('title');
						
						doc.addImage(img, 'JPEG', 10, 5);
						doc.save(title + '.pdf');
						
						$('canvas').remove();
					}
				});
			}
		</script>

		<style media="print">
			.no-print, .no-print * {
				display: none !important;
			}
			.break {
				page-break-after: always;
			}
		</style>

		<style media="screen">
			body {
				margin: 0px;
				font-family: "Roboto";
				width: 190mm;
			}
			#container {
				font-family: "Times New Roman", Times, serif;
			}
			.aksi {
				background-color: #000;
				text-align: right;
			}
			.aksi a {
				display: inline-block;
				padding: 12px 15px;
				color: #fff;
				text-decoration: none;
				background-color: #7f8c8d;
				margin-right: -3px;
			}
			.aksi a:hover {
				background-color: #2c3e50
			}
			.wrapper {
				padding: 15px;
			}
			table {
				border-collapse: collapse;
				width: 100%
			}
			table th, table td {
				border: 1px solid #000;
			}
			table th {
				background-color: #ecf0f1
			}
			table td {
				vertical-align: top;
				padding: 2px;
			}
			table td p {
				margin-top: 0px;
			}
			table.no-border th, table.no-border td {
				border: 0px !important;
			}
			ol, ul {
				padding-left: 18px !important;
			}
		</style>
	</head>
	<body>
		<div class="aksi no-print">
			<a class="btn-cetak" href="#">CETAK</a>
			<a class="btn-export-pdf" href="#">DOWNLOAD PDF</a>
		</div>

		<div id="container" class="wrapper">
			<?php echo $output ?>
		</div>
	</body>
</html>
