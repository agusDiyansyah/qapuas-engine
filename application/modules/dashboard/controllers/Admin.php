<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller {
	private $title = "dashboard";
	private $module = "dashboard/admin";
	
	// PUBLIC
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("M_klasifikasi", "M_app");
			$this->output->script_foot_string("
				$(document).ready(function () {
					$('.mn-dashboard').addClass('active');
				});
			");
			
			if ($this->input->is_ajax_request()) {
				$this->output->unset_template();
			}
			
		}

		public function index () {
			// datatables
			$this->output->js('assets/themes/admin/vendors/data-table/DataTables-1.10.18/js/jquery.dataTables.min.js');
			$this->output->js('assets/themes/admin/vendors/data-table/DataTables-1.10.18/js/dataTables.bootstrap4.min.js');
			$this->output->css('assets/themes/admin/vendors/data-table/DataTables-1.10.18/css/dataTables.bootstrap4.min.css');
			
			// select2
			$this->output->css("assets/themes/admin/vendors/select2/select2.css");
			$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
			$this->output->js("assets/themes/admin/vendors/select2/select2.js");
			
			// custom
			$this->output->script_foot("$this->module/assets/data.js");
			
			$data = array(
				"title" => $this->title,
				"subtitle" => "List Data",
				"link_data" => site_url("$this->module/"),
				"link_form" => site_url("$this->module/form"),
				
				"filter" => array(
					"kode" => form_input(array(
						"name" => "kode",
						"class" => "form-control kode",
						"type" => "text",
						"value" => "",
					)),
					"klasifikasi" => form_input(array(
						"name" => "klasifikasi",
						"class" => "form-control klasifikasi",
						"type" => "text",
						"value" => "",
					)),
				),
			);
			
			$this->load->view("$this->module/data", $data);
		}
}