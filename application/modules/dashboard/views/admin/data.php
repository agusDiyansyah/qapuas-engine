<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
		<!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
			<i class="fas fa-download fa-sm text-white-50"></i> 
			Generate Report
		</a> -->
	</div>

	<div class="card shadow mb-4">
		<div class="card-header">
			&nbsp
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-12">
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia quia neque cumque sequi voluptas minima ipsa! Voluptatum eos nulla porro et, dolor ea autem, quae, commodi labore provident rem. Quod?
				</div>
			</div>
		</div>
		<div class="card-footer">
			&nbsp
		</div>
	</div>

</div>