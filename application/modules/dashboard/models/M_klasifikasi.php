<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_klasifikasi extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	
	public function data ($post, $debug = false) {
		
		$order = $post['order'][0];

		$this->db->start_cache();
		
			$this->db->from("klasifikasi k");

			// filter
			if (!empty($post['klasifikasi'])) {
				$this->db->like("klasifikasi", $post['klasifikasi'], "both");
			}
			if (!empty($post['kode'])) {
				$this->db->like("kode", $post['kode'], "both");
			}
			
			$orderColumn = array(
				2 => "kode",
				3 => "klasifikasi",
			);
			
			// order
			if ($order['column'] == 0) {
				$this->db->order_by('id_klasifikasi', $order['dir']);
			} else {
				$this->db->order_by($orderColumn[$order['column']], $order['dir']);
			}

			// join

		$this->db->stop_cache();

		// get num rows
		$this->db->select('id_klasifikasi');
		$rowCount = $this->db->get()->num_rows();

		// get result
		$this->db->select('k.*');

		$this->db->limit($post['length'], $post['start']);

		$val = $this->db->get()->result();

		$this->db->flush_cache();
		
		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$output['draw']            = $post['draw'];
		$output['recordsTotal']    = $rowCount;
		$output['recordsFiltered'] = $rowCount;
		$output['data']            = array();

		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$no = 1 + $post['start'];

		$base = base_url();
		
		foreach ($val as $data) {
			
			$btnAksi = "";
			
			$btnAksi .= "
			<li>
				<a href='{$base}klasifikasi/admin/form/$data->id_klasifikasi' class='btn-edit'>
					EDIT
				</a>
			</li>
			";
			
			$btnAksi .= "
			<li>
				<a href='#' data-id='$data->id_klasifikasi' class='btn-hapus'>
					HAPUS
				</a>
			</li>
			";
						
			$aksi = "
			<div class='btn-group tb-aksi-wrp'>
				<button type='button' class='btn dropdown-toggle btn-default' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<i class='fa fa-gear'></i>
				</button>
				<ul class='dropdown-menu'>
					$btnAksi
				</ul>
			</div>
			";

			$baris = array(
				"no" => $no,
				"aksi" => $aksi,
				"kode" => $data->kode,
				"klasifikasi" => $data->klasifikasi,
			);

			array_push($output['data'], $baris);
			$no++;
		}
		return json_encode($output);
	}
	
	public function add ($data) {
		$this->db->insert("klasifikasi", $data);
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id) {
		return $this->db
			->where("id_klasifikasi", $id)
			->update("klasifikasi", $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where("id_klasifikasi", $id)
			->delete("klasifikasi");
	}
		
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where("id_klasifikasi", $id)
			->get("klasifikasi");
	}
}

/* End of file M_asb.php */
/* Location: ./application/modules/asb/models/M_asb.php */