<div class="bg-white w-aksi pt-3 pb-3 pl-4 pr-4 mb-4">
	<div class="d-sm-flex align-items-center justify-content-between">
		<div class="pt-1 pl-1">
			<h1 class="h6 m-0 text-gray-800 font-weight-bold d-inline-block"><?php echo strtoupper($title) ?></h1>
			<span class="span-titik"></span>
			<span class="muted"><i class="fa fa-file-text-o"></i>&nbsp Datasource</span>
		</div>

		<div>
			<div class="btn-group" role="group" aria-label="Basic example">
				<a href="#" class="d-sm-inline-block btn btn-sm btn-secondary shadow-sm">
					Laporan
				</a>
				<a href="#" class="d-sm-inline-block btn btn-sm shadow-sm btn-info btn-filter">
					Pencarian Rinci
				</a>
			</div>
			<a href="<?php echo $link_form ?>" class="d-sm-inline-block btn btn-sm btn-success shadow-sm">
				Tambah Data
			</a>
		</div>
	</div>
</div>

<div class="container-fluid">

	<?php echo $this->session->flashdata('message'); ?>

	<div class="card mb-4">
		<!-- <div class="card-header bg-white">
			
		</div> -->
		<div>
			<form action="#" class="form-filter d-none" style="padding: 20px 20px 5px">
				<div class="row mb-3">
					<div class="col-md-6">
						<div class="form-group">
							<label for="" class="control-label">Kode</label>
							<?php echo $filter['kode'] ?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label for="" class="control-label">Klasifikasi</label>
							<?php echo $filter['klasifikasi'] ?>
						</div>
					</div>

					<div class="col-md-12 text-right">
						<div class="btn-group" role="group" aria-label="Basic example">
							<button class="btn btn-info btn-sm" type="submit">Filter Data</button>
							<a href="#" class="btn btn-success btn-sm">Reset Pencarian</a>
						</div>
					</div>
				</div>
			</form>

			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<td style="width: 2%"></td>
									<td style="width: 2%">Aksi</td>
									<td>Kode</td>
									<td>Uraian</td>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer bg-white">
			&nbsp
		</div>
	</div>

</div>