$(document).ready(function() {	
	var table = $('.table');
	var filter = $('.form-filter');
	
	table.DataTable({
		"ajax": {
			"url"    :"<?php echo base_url('klasifikasi/admin/data') ?>",
			"method" :"POST",
			"data"   : function ( d ) {
				d.kode = filter.find('.kode').val();
				d.klasifikasi = filter.find('.klasifikasi').val();
			}
		},
		"columns": [
			{"data": "no"},
			{"data": "aksi"},
			{"data": "kode"},
			{"data": "klasifikasi"},
		],

		"pageLength"  : 100,
		"deferRender" : true,
		"serverSide"  : true,
		"processing"  : false,
		"filter"      : false,
		"ordering"    : true,
		"bLengthChange": false,

		"order": [[ 0, "desc" ]],
		
		"drawCallback": function () {
			$("[data-toggle='tooltip']").tooltip();
		},

		"columnDefs": [
			{ "targets": 0, "orderable": false, "className": "text-right" },
			{ "targets": 1, "orderable": false },
		],

		"language": {
			"sProcessing"   : "Sedang memproses...",
			"sLengthMenu"   : "Tampilkan _MENU_ entri",
			"sZeroRecords"  : "Tidak ditemukan data",
			"sInfo"         : "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
			"sInfoEmpty"    : "Menampilkan 0 sampai 0 dari 0 entri",
			"sInfoFiltered" : "(difilter dari _MAX_ entri keseluruhan)",
			"sInfoPostFix"  : "",
			"sUrl"          : "",
			"oPaginate"     : {
				"sFirst"        : "Pertama",
				"sPrevious"     : "<<",
				"sNext"         : ">>",
				"sLast"         : "Terakhir"
			}
		},
	});
	
	table.on('click', '.btn-hapus', function(event) {
		event.preventDefault();
		var id = $(this).data('id');
		hapus_data(id);
	});
	
	filter.on('click', '.btn-filter', function(event) {
		event.preventDefault();
		filter.submit();
	});
	
	filter.on('click', '.btn-reset', function(event) {
		event.preventDefault();

		$('.form-control').val('');
		
		refreshTable();
	});
	
	filter.on('submit', function(event) {
		event.preventDefault();
		refreshTable();
	});
	
	$('.s2').select2({
		width: '100%'
	});
});

function hapus_data (id) {
	swal({
		title: "HAPUS DATA",
		text: "Anda yakin akan menghapus data ini",
		showCancelButton: true,
		confirmButtonColor: "#ff5252",
		confirmButtonText: "HAPUS DATA",
		cancelButtonText: "BATALKAN",
		showLoaderOnConfirm: true,
		closeOnConfirm: true,
		closeOnCancel: true
	},
	function (isConfirm) {
		if (isConfirm) {
			$.ajax({
				url: "<?php echo base_url('klasifikasi/admin/delete_proses') ?>",
				type: "post",
				dataType: "json",
				data: { id: id },
				success: function (json) {
					if (json.stat) {
						refreshTable();
					}
					alert(json.msg);
				}
			});
		}
	});
	
	$('.sweet-alert button.cancel').css('background-color', '#3C4248');
}

function refreshTable () {
	var dtable = $(".table").DataTable();
	dtable.draw(false);
}