$(document).ready(function() {
	$('.btn-proses').on('click', function(event) {
		event.preventDefault();
		$('.form').submit();
	});
	
	$('.s2').select2({
		width: '100%'
	});
	
	$('.form').validate({
		ignore: [],
		errorClass: "error",
		invalidHandler: function(form, validator) {
			var errors = validator.numberOfInvalids();

			if (errors) {
				var errors = "";

				if (validator.errorList.length > 0) {
					for (x = 0; x < validator.errorList.length; x++) {
						errors += "<div class='text-danger'>" + validator.errorList[x].message + "</div>";
					}
				}
				swal({
					title: "ERROR",
					text: errors,
					// type: "error",
					confirmButtonColor: "#fb483a",
					html: true
				});
			}
			validator.focusInvalid();
		},
		rules: {
			kode: { required: true },
			klasifikasi: { required: true },
		},
		messages: {
			kode: { required: 'Kode klasifikasi tidak boleh kosong' },
			klasifikasi: { required: 'klasifikasi tidak boleh kosong' },
		},
		errorPlacement: function (error, element) {
			error.insertAfter(element);
		},
		highlight: function (element, validClass) {
			$(element).parent().addClass('has-error');
		},
		unhighlight: function (element, validClass) {
			$(element).parent().removeClass('has-error');
		},
	});
});
