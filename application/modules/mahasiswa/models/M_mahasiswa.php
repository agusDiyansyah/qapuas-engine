<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_mahasiswa extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	
	public function data ($post, $debug = false) {
		
		$order = $post['order'][0];

		$this->db->start_cache();
		
			$this->db->from("mahasiswa m");

			// filter
			if (!empty($post['mahasiswa'])) {
				$this->db->like("mahasiswa", $post['mahasiswa'], "both");
			}
			if (!empty($post['kode'])) {
				$this->db->like("kode", $post['kode'], "both");
			}
			
			$orderColumn = array(
				2 => "kode",
				3 => "mahasiswa",
			);
			
			// order
			if ($order['column'] == 0) {
				$this->db->order_by('id_mahasiswa', $order['dir']);
			} else {
				$this->db->order_by($orderColumn[$order['column']], $order['dir']);
			}

			// join

		$this->db->stop_cache();

		// get num rows
		$this->db->select('id_mahasiswa');
		$rowCount = $this->db->get()->num_rows();

		// get result
		$this->db->select('m.*');

		$this->db->limit($post['length'], $post['start']);

		$val = $this->db->get()->result();

		$this->db->flush_cache();
		
		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$output['draw']            = $post['draw'];
		$output['recordsTotal']    = $rowCount;
		$output['recordsFiltered'] = $rowCount;
		$output['data']            = array();

		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$no = 1 + $post['start'];

		$base = base_url();
		
		foreach ($val as $data) {
			
			$btnAksi = "";
			
			$btnAksi .= "
			<a href='{$base}mahasiswa/admin/form/$data->id_mahasiswa' class='dropdown-item btn-edit'>
				EDIT
			</a>
			";
			
			$btnAksi .= "
			<a href='#' data-id='$data->id_mahasiswa' class='dropdown-item btn-hapus'>
				HAPUS
			</a>
			";

			$aksi = "
			<div class='dropdown'>
				<button class='btn btn-outline-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<i class='fa fa-cog'></i>
				</button>
				<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
					$btnAksi
				</div>
			</div>
			";

			$baris = array(
				"no" => $no,
				"aksi" => $aksi,
				"nama" => $data->nama,
				"kelas" => $data->kelas,
			);

			array_push($output['data'], $baris);
			$no++;
		}
		return json_encode($output);
	}
	
	public function add ($data) {
		$this->db->insert("mahasiswa", $data);
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id) {
		return $this->db
			->where("id_mahasiswa", $id)
			->update("mahasiswa", $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where("id_mahasiswa", $id)
			->delete("mahasiswa");
	}
		
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where("id_mahasiswa", $id)
			->get("mahasiswa");
	}
}

/* End of file M_asb.php */
/* Location: ./application/modules/asb/models/M_asb.php */