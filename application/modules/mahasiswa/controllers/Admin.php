<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller {
	private $title = "Mahasiswa";
	private $module = "mahasiswa/admin";
	
	// PUBLIC
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("M_mahasiswa", "M_app");
			$this->output->script_foot_string("
				$(document).ready(function () {
					$('.mn-mahasiswa').addClass('active');
				});
			");
			
			if ($this->input->is_ajax_request()) {
				$this->output->unset_template();
			}
			
		}

		public function index () {
			// datatables
			$this->output->js('assets/themes/admin/vendors/data-table/DataTables-1.10.18/js/jquery.dataTables.min.js');
			$this->output->js('assets/themes/admin/vendors/data-table/DataTables-1.10.18/js/dataTables.bootstrap4.min.js');
			$this->output->css('assets/themes/admin/vendors/data-table/DataTables-1.10.18/css/dataTables.bootstrap4.min.css');
			
			// select2
			$this->output->css("assets/themes/admin/vendors/select2/select2.css");
			$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
			$this->output->js("assets/themes/admin/vendors/select2/select2.js");
			
			// custom
			$this->output->script_foot("$this->module/assets/data.js");
			
			$data = array(
				"title" => $this->title,
				"link_form" => site_url("$this->module/form"),
				
				"filter" => array(
					"kode" => form_input(array(
						"name" => "kode",
						"class" => "form-control kode",
						"type" => "text",
						"value" => "",
					)),
					"mahasiswa" => form_input(array(
						"name" => "mahasiswa",
						"class" => "form-control mahasiswa",
						"type" => "text",
						"value" => "",
					)),
				),
			);
			
			$this->load->view("$this->module/data", $data);
		}
		
		public function data () {
			$this->output->unset_template();
			header("Content-type: application/json");
			if(
				isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
				)
			{
				echo $this->M_app->data($_POST);
			}
			return;
		}
		
		public function form ($id = 0) {
			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
			
			// select2
			$this->output->css("assets/themes/admin/vendors/select2/select2.css");
			$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
			$this->output->js("assets/themes/admin/vendors/select2/select2.js");

			// custom
			$this->output->script_foot("$this->module/assets/form.js");

			if ($id > 0) {
				$sql = $this->db
					->from('mahasiswa')
					->where('id_mahasiswa', $id)
					->get();

				$val = $sql->row();
			}

			$data = array(
				"link_proses" => base_url("$this->module/proses"),
				"link_data" => base_url("$this->module/"),
				"link_form" => "#",
				"title" => @$this->title,
				"subtitle" => (!empty($id) ? "Edit" : "Tambah") . " mahasiswa",

				"form" => array(
					"open" => form_open(
						"$this->module/proses",
						array(
							'class' => 'form',
							// 'enctype' => 'multipart/form-data',
							// 'target' => '_blank',
						),
						array(
							'id' => @$val->id_mahasiswa,
						)
					),
					"close" => form_close(),

					"nama" => form_input(array(
						"name" => "nama",
						"class" => "form-control nama",
						"type" => "text",
						"value" => @$val->nama,
					)),
					"kelas" => form_input(array(
						"name" => "kelas",
						"class" => "form-control kelas",
						"type" => "text",
						"value" => @$val->kelas,
					)),
				)
			);

			$this->load->view("$this->module/form", $data);
		}
		
		public function proses () {
			if ($this->input->post()) {
				$id = $this->input->post("id");
				
				$stat = false;
				$back = "$this->module/form/" . @$id;
				$submsg = "Data gagal di proses";
				
				$rules = $this->_rules();
				
				if (!$rules['stat']) {
					$submsg = $rules['msg'];
				} else {
					$data = $this->_formPostInputData();
					
					if (empty($id)) {
						// add proses
						$proses = $this->M_app->add($data);
					} else {
						// edit proses
						$proses = $this->M_app->edit($data, $id);
					}
					
					if ($proses) {
						$stat = true;
						$back = $this->module;
						$submsg = "Data berhasil di proses";
					}
				}
				
				$this->_notif($back, $submsg, $stat);
			} else {
				show_404();
			}
		}
	
		public function delete_proses () {
			if ($this->input->post('id')) {
				$id = $this->input->post('id');
				$sql = $this->M_app->cekId($id, 'id_mahasiswa');
				$stat = false;
				$msg = 'Data gagal dihapus';

				if ($sql->num_rows() > 0) {
					$del = $this->M_app->delete($id);

					if ($del) {
						$stat = true;
						$msg = 'Data berhasil dihapus';
					}
				}

				echo json_encode(array(
					"stat" => $stat,
					"msg" => $msg,
				));

			} else {
				show_404();
			}
		}
	
	// PRIVATE
		private function _formPostInputData () {
			// header('content-type: application/json');
			// echo json_encode($_POST); die;
			$ret = array(
				'kode' => $this->input->post('kode'),
				'mahasiswa' => $this->input->post('mahasiswa'),
			);
			
			return $ret;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "mahasiswa",
					"label" => "mahasiswa",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "kode",
					"label" => "Kode mahasiswa",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);

			$err = "";
			
			if (form_error("kode")) {
				$err .= form_error("kode");
			}
			if (form_error("mahasiswa")) {
				$err .= form_error("mahasiswa");
			}
			
			return array(
				"stat" => $this->form_validation->run(),
				"msg" => $err,
			);
		}
		
		private function _notif ($back, $submsg = "", $stat = false) {
			if ($stat) {
				$this->session->set_flashdata(
					"message",
					"
						<div class='alert alert-success alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span>
							</button>
							<b class='d-block'>BERHASIL</b>
							<span>$submsg</span>
						</div>
					"
				);
			} else {
				$this->session->set_flashdata(
					"message",
					"
						<div class='alert alert-danger alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span>
							</button>
							<b class='d-block'>GAGAL</b>
							<span>$submsg</span>
						</div>
					"
				);
			}
			
			redirect($back);
		}
}