$(document).ready(function () {
    var preview =  <?php echo json_encode($preview) ?>;
    var preview_conf =  <?php echo json_encode($preview_conf) ?>;

    var footerTemplate = '' +
    '<div class="file-thumbnail-footer" style ="height:94px">' +
    '   <input class="form-control" name="tes[]" value="{val}" placeholder="">' +
    // '   <input class="form-control" value="" placeholder="">' +
    '   <div class="small" style="margin:15px 0 2px 0">' +
    '       {size}' +
    '   </div>' +
    '   {progress}' +
    '   {indicator}' +
    '   {actions}' +
    '</div>';

    $(".galeri_file").fileinput({
		theme: 'fa',
		showCaption: false,
		showBrowse: false,
		showRemove: false,
		showUpload: false,
        initialPreviewAsData: true,
        overwriteInitial: false,
        previewFileType: 'any',
        initialPreview: preview,
		initialPreviewConfig: preview_conf,
        deleteUrl: "<?= base_url() ?>",
        layoutTemplates: {footer: footerTemplate},
        initialPreviewThumbTags: [
            {'{val}' : 'asasas'},
            {'{val}' : 'sdsd'},
            {'{val}' : 'asasasaasas'},
            {'{val}' : 'jh'},
        ],
        previewThumbTags: {
            '{val}': '',  // hide the initial input
        },
	});
});