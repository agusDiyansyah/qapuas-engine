<div class="bg-white w-aksi pt-3 pb-3 pl-4 pr-4 mb-4">
	<div class="d-sm-flex align-items-center justify-content-between">
		<div class="pt-1 pl-1">
			<h1 class="h6 m-0 text-gray-800 font-weight-bold d-inline-block"><?php echo strtoupper($title) ?></h1>
			<span class="span-titik"></span>
			<span class="muted"><i class="fa fa-file-text-o"></i>&nbsp Form</span>
		</div>

		<div>
			<a href="<?php echo $link_data ?>" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm">
				Kembali
			</a>
		</div>
	</div>
</div>

<div class="container-fluid">

	<?php echo $this->session->flashdata('message'); ?>

	<div class="card mb-4">
		<!-- <div class="card-header bg-white">
			
		</div> -->
		<div class="p-4">
			<?php echo $form['open'] ?>
				<div class="row">
					
					<!-- Work in here -->
					<div class="col-md-12">
						<div class="form-group">
							<label for="" class="control-label">Nama Galeri</label>
							<?php echo $form['galeri'] ?>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label for="" class="control-label">File</label>
							<?php echo $form['galeri_file'] ?>
						</div>
					</div>
					<!-- until here -->

					<div class="col-md-12 text-right">
						<button class="btn btn-success btn-sm" type="submit">
							Proses
						</button>
					</div>

				</div>
			<?php echo $form['close'] ?>
		</div>
		<div class="card-footer bg-white">
			&nbsp
		</div>
	</div>

</div>