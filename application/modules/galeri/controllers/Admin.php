<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller {
	private $title = "Galeri";
	private $module = "galeri/admin";
	
	// PUBLIC
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("M_galeri", "M_app");
			$this->output->script_foot_string("
				$(document).ready(function () {
					$('.mn-galeri').addClass('active');
				});
			");
			
			if ($this->input->is_ajax_request()) {
				$this->output->unset_template();
			}
			
		}

		public function index () {
			// datatables
			$this->output->js('assets/themes/admin/vendors/data-table/DataTables-1.10.18/js/jquery.dataTables.min.js');
			$this->output->js('assets/themes/admin/vendors/data-table/DataTables-1.10.18/js/dataTables.bootstrap4.min.js');
			$this->output->css('assets/themes/admin/vendors/data-table/DataTables-1.10.18/css/dataTables.bootstrap4.min.css');
			
			// select2
			$this->output->css("assets/themes/admin/vendors/select2/select2.css");
			$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
			$this->output->js("assets/themes/admin/vendors/select2/select2.js");
			
			// custom
			$this->output->script_foot("$this->module/assets/data.js");
			
			$data = array(
				"title" => $this->title,
				"link_form" => site_url("$this->module/form"),
				
				"filter" => array(
					"galeri" => form_input(array(
						"name" => "galeri",
						"class" => "form-control galeri",
						"type" => "text",
						"value" => "",
					)),
				),
			);
			
			$this->load->view("$this->module/data", $data);
		}
		
		public function data () {
			$this->output->unset_template();
			header("Content-type: application/json");
			if(
				isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
				)
			{
				echo $this->M_app->data($_POST);
			}
			return;
		}
		
		public function form ($id = 0) {
			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
			
			// select2
			$this->output->css("assets/themes/admin/vendors/select2/select2.css");
			$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
			$this->output->js("assets/themes/admin/vendors/select2/select2.js");

			// fileinput
			$this->output->css("assets/themes/admin/vendors/bootstrap-fileinput-master/css/fileinput.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-fileinput-master/js/plugins/sortable.js");
			$this->output->js("assets/themes/admin/vendors/bootstrap-fileinput-master/js/plugins/piexif.js");
			$this->output->js("assets/themes/admin/vendors/bootstrap-fileinput-master/js/fileinput.js");
			$this->output->js("assets/themes/admin/vendors/bootstrap-fileinput-master/themes/fa/theme.js");

			// custom
			$this->output->script_head("$this->module/assets/fileinput-custom.css");
			$this->output->script_foot("$this->module/assets/form.js");

			if ($id > 0) {
				$sql = $this->db
					->from('galeri')
					->where('id_galeri', $id)
					->get();

				$val = $sql->row();
			}
			
			$preview = [
				base_url('assets/themes/admin/img/niilo-isotalo-379496.jpg'),
				base_url('assets/themes/admin/img/benjamin-davies-332625.jpg'),
				base_url('assets/themes/admin/img/anton-repponen-103080.jpg'),
			];

			$preview_conf = [
				[
					"downloadUrl" => base_url('assets/themes/admin/img/niilo-isotalo-379496.jpg'),
					"key" => 1,
					"size" => 1,
					"caption" => "Image",
				],
				[
					"downloadUrl" => base_url('assets/themes/admin/img/benjamin-davies-332625.jpg'),
					"key" => 2,
					"size" => 1,
					"caption" => "Image",
				],
				[
					"downloadUrl" => base_url('assets/themes/admin/img/anton-repponen-103080.jpg'),
					"key" => 3,
					"size" => 1,
					"caption" => "Image",
				],
			];

			$fileinput_parse = [
				"preview" => $preview,
				"preview_conf" => $preview_conf,
			];
			$this->output->script_foot("$this->module/assets/fileinput-custom.js", $fileinput_parse);

			$data = array(
				"link_proses" => base_url("$this->module/proses"),
				"link_data" => base_url("$this->module/"),
				"link_form" => "#",
				"title" => @$this->title,
				"subtitle" => (!empty($id) ? "Edit" : "Tambah") . " galeri",

				"form" => array(
					"open" => form_open(
						"$this->module/proses",
						array(
							'class' => 'form',
							'enctype' => 'multipart/form-data',
							// 'target' => '_blank',
						),
						array(
							'id' => @$val->id_galeri,
						)
					),
					"close" => form_close(),

					"galeri" => form_input(array(
						"name" => "galeri",
						"class" => "form-control galeri",
						"type" => "text",
						"value" => @$val->galeri,
					)),
					"galeri_file" => form_input(array(
						"name" => "galeri_file[]",
						"multiple" => true,
						"class" => "form-control galeri_file",
						"type" => "file",
					)),
				)
			);

			$this->load->view("$this->module/form", $data);
		}
		
		public function proses () {
			header('content-type: application/json');
			echo json_encode($_FILES + $_POST);
			die;

			if ($this->input->post()) {
				$id = $this->input->post("id");
				
				$stat = false;
				$back = "$this->module/form/" . @$id;
				$submsg = "Data gagal di proses";
				
				$rules = $this->_rules();
				
				if (!$rules['stat']) {
					$submsg = $rules['msg'];
				} else {
					$data = $this->_formPostInputData();
					
					if (empty($id)) {
						// add proses
						$proses = $this->M_app->add($data);
					} else {
						// edit proses
						$proses = $this->M_app->edit($data, $id);
					}
					
					if ($proses) {
						$stat = true;
						$back = $this->module;
						$submsg = "Data berhasil di proses";
					}
				}
				
				$this->_notif($back, $submsg, $stat);
			} else {
				show_404();
			}
		}
	
		public function delete_proses () {
			if ($this->input->post('id')) {
				$id = $this->input->post('id');
				$sql = $this->M_app->cekId($id, 'id_galeri');
				$stat = false;
				$msg = 'Data gagal dihapus';

				if ($sql->num_rows() > 0) {
					$del = $this->M_app->delete($id);

					if ($del) {
						$stat = true;
						$msg = 'Data berhasil dihapus';
					}
				}

				echo json_encode(array(
					"stat" => $stat,
					"msg" => $msg,
				));

			} else {
				show_404();
			}
		}
	
	// PRIVATE
		private function _formPostInputData () {
			// header('content-type: application/json');
			// echo json_encode($_POST); die;
			$ret = array(
				'kode' => $this->input->post('kode'),
				'galeri' => $this->input->post('galeri'),
			);
			
			return $ret;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "galeri",
					"label" => "galeri",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				array(
					"field" => "kode",
					"label" => "Kode galeri",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);

			$err = "";
			
			if (form_error("kode")) {
				$err .= form_error("kode");
			}
			if (form_error("galeri")) {
				$err .= form_error("galeri");
			}
			
			return array(
				"stat" => $this->form_validation->run(),
				"msg" => $err,
			);
		}
		
		private function _notif ($back, $submsg = "", $stat = false) {
			if ($stat) {
				$this->session->set_flashdata(
					"message",
					"
						<div class='alert alert-success alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span>
							</button>
							<b class='d-block'>BERHASIL</b>
							<span>$submsg</span>
						</div>
					"
				);
			} else {
				$this->session->set_flashdata(
					"message",
					"
						<div class='alert alert-danger alert-dismissable'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span>
							</button>
							<b class='d-block'>GAGAL</b>
							<span>$submsg</span>
						</div>
					"
				);
			}
			
			redirect($back);
		}
}