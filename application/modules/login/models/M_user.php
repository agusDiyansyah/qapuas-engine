<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	
	public function data ($post, $debug = false) {
		
		$order = $post['order'][0];

		$this->db->start_cache();
		
			$this->db->from("admin r");

			// filter
			if (!empty($post['nama_lengkap'])) {
				$this->db->like("nama_lengkap", $post['nama_lengkap'], "both");
			}
			if ( $post['username'] != "" ) {
				$this->db->like("username", $post['username'], "both");
			}
			if (!empty($post['id_level'])) {
				$this->db->where('r.id_level', $post['id_level']);
			}
			if (!empty($post['id_skpd'])) {
				$this->db->where('r.id_skpd', $post['id_skpd']);
			}
			
			$orderColumn = array(
				2 => "nama_lengkap",
				3 => "username",
				4 => "l.level",
				5 => "s.skpd",
			);
			
			// order
			if ($order['column'] == 0) {
				$this->db->order_by('id_admin', $order['dir']);
			} else {
				$this->db->order_by($orderColumn[$order['column']], $order['dir']);
			}

			// join
			$this->db->join('admin_level l', 'l.id_level = r.id_level', 'left');
			$this->db->join('skpd s', 's.id_skpd = r.id_skpd', 'left');

		$this->db->stop_cache();

		// get num rows
		$this->db->select('id_admin');
		$rowCount = $this->db->get()->num_rows();

		// get result
		$this->db->select('r.*, l.*, s.*');

		$this->db->limit($post['length'], $post['start']);

		$val = $this->db->get()->result();
		// echo $this->db->last_query(); die();

		$this->db->flush_cache();
		
		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$output['draw']            = $post['draw'];
		$output['recordsTotal']    = $rowCount;
		$output['recordsFiltered'] = $rowCount;
		$output['data']            = array();

		if ($debug) {
			$output['sql'] = $this->db->last_query();
		}

		$no = 1 + $post['start'];

		$base = base_url();
		
		foreach ($val as $data) {
			
			$btnAksi = "";
			
			$btnAksi .= "
			<li>
				<a href='{$base}admin/form/$data->id_admin' id='btn-detail'>
					EDIT
				</a>
			</li>
			";
			
			$btnAksi .= "
			<li>
				<a href='#' data-id='$data->id_admin' id='btn-delete'>
					HAPUS
				</a>
			</li>
			";
						
			$aksi = "
			<div class='btn-group tb-aksi-wrp'>
				<button type='button' class='btn dropdown-toggle btn-default' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<i class='fa fa-gear'></i>
				</button>
				<ul class='dropdown-menu'>
					$btnAksi
				</ul>
			</div>
			";

			$baris = array(
				"no" => $no,
				"aksi" => $aksi,
				"nama_lengkap" => $data->nama_lengkap,
				"username" => $data->username,
				"level" => $data->level,
				"skpd" => $data->skpd,
			);

			array_push($output['data'], $baris);
			$no++;
		}
		return json_encode($output);
	}
	
	public function add ($data) {
		$this->db->insert("admin", $data);
		return $this->db->insert_id();
	}
	
	public function edit ($data, $id) {
		return $this->db
			->where("id_admin", $id)
			->update("admin", $data);
	}
	
	public function delete ($id) {
		return $this->db
			->where("id_admin", $id)
			->delete("admin");
	}
		
	public function cekId ($id, $select = "*") {
		return $this->db
			->select($select)
			->where("id_admin", $id)
			->get("admin");
	}

	function check_username($username, $id) {
		$this->db->select('username');
		$this->db->from('admin');
		$this->db->where(array("username" => $username));
		if ($id > 0) {
			$this->db->where("id_admin !=", $id);
		}
		$query = $this->db->get();

		if ($query->num_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function getLevel () {
		$sql = $this->db->get('admin_level');

		$arr = array();
		foreach ($sql->result() as $data) {
			$arr += array(
				$data->id_level => $data->level
			);
		}

		return $arr;
	}

	public function getSkpd () {
		$sql = $this->db->get('skpd');

		$arr = array();
		foreach ($sql->result() as $data) {
			$arr += array(
				$data->id_skpd => $data->skpd
			);
		}

		return $arr;
	}

}

/* End of file M_admin.php */
/* Location: ./application/modules/admin/models/M_admin.php */