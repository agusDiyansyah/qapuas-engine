<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Admin_controller {

	// Define
		public $title = "Admin";
		private $module = "admin/user";
		private $upload_path = "./assets/upload/admin";
		private $stat = false;

	// Public
		public function __construct () {
			parent::__construct ();

			$this->load->model("M_user", "M_app");
		}

		public function index () {
			$this->auth->grant(1);
			
			// datatables
			$this->output->js('assets/themes/admin/vendors/data-table/DataTables-1.10.18/js/jquery.dataTables.min.js');
			$this->output->js('assets/themes/admin/vendors/data-table/DataTables-1.10.18/js/dataTables.bootstrap4.min.js');
			$this->output->css('assets/themes/admin/vendors/data-table/DataTables-1.10.18/css/dataTables.bootstrap4.min.css');

			// select2
			$this->output->css("assets/themes/admin/vendors/select2/select2.css");
			$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
			$this->output->js("assets/themes/admin/vendors/select2/select2.js");

			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');

			// custom
			$this->output->script_foot("$this->module/js/data.js");

			$data = array(
				"link_data" => "#",
				"link_form" => base_url("$this->module/form"),
				"title" => $this->title,
				"filter" => array(
					"username" => form_input(array(
						"name" => "username",
						"class" => "form-control username",
						"type" => "text",
					)),
					"nama_lengkap" => form_input(array(
						"name" => "nama_lengkap",
						"class" => "form-control nama_lengkap",
						"type" => "text",
					)),
					"id_level" => form_select(array(
						"config" => array(
							"name" => "id_level",
							"class" => "form-control s2 id_level",
						),
						"list" => array("" => "") + $this->M_app->getLevel(),
					)),
					"id_skpd" => form_select(array(
						"config" => array(
							"name" => "id_skpd",
							"class" => "form-control s2 id_skpd",
						),
						"list" => array("" => "") + $this->M_app->getSkpd(),
					)),
				),
			);

			$this->load->view("$this->module/data", $data);
		}

		public function data () {
			$this->output->unset_template();
			header("Content-type: application/json");
			if(
				isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
				strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
				)
			{
				echo $this->M_app->data($_POST);
			}
			return;
		}

		public function form ($id = 0) {
			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');

			// FILEINPUT
			$this->output->css('assets/themes/admin/vendors/file-input/css/fileinput.css');
			$this->output->css('assets/themes/admin/vendors/file-input/css/custom-file-input.css');
			$this->output->js('assets/themes/admin/vendors/file-input/js/fileinput.js');
			$this->output->js('assets/themes/admin/vendors/file-input/themes/fa/theme.js');

			// select2
			$this->output->css("assets/themes/admin/vendors/select2/select2.css");
			$this->output->css("assets/themes/admin/vendors/select2/custom-select2.css");
			$this->output->js("assets/themes/admin/vendors/select2/select2.js");

			// custom
			$this->output->script_foot("$this->module/js/form.js");

			if ($id > 0) {
				$sql = $this->M_app->cekid($id);

				$val = $sql->row();

				$avatar = (!empty($val->avatar) && file_exists(FCPATH . "$this->upload_path/$val->avatar")) ? $val->avatar : "";
			}

			$data = array(
				"proses" => base_url("$this->module/proses"),
				"link_data" => base_url("$this->module/"),
				"link_form" => "#",

				"title" => @$this->title,
				"subtitle" => (!empty($id) ? "Edit" : "Tambah") . " Data",

				"avatar" => @$avatar,

				"form" => array(
					"hidden" => array(
						"id" => form_input(array(
							"name" => "id",
							"class" => "form-control id",
							"type" => "hidden",
							"value" => @$id,
						)),
					),

					"nama_lengkap" => form_input(array(
						"name" => "nama_lengkap",
						"value" => @$val->nama_lengkap,
						"class" => "form-control nama_lengkap",
						"type" => "text",
					)),
					"username" => form_input(array(
						"name" => "username",
						"value" => @$val->username,
						"class" => "form-control username",
						"type" => "text",
					)),
					"pass" => form_input(array(
						"name" => "pass",
						"class" => "form-control pass",
						"type" => "password",
					)),
					"pass_confirm" => form_input(array(
						"name" => "pass_confirm",
						"class" => "form-control pass_confirm",
						"type" => "password",
					)),
					"id_level" => form_select(array(
						"config" => array(
							"name" => "id_level",
							"class" => "form-control s2 id_level",
						),
						"list" => array("" => "") + $this->M_app->getLevel(),
						"selected" => @$val->id_level,
					)),
					"id_skpd" => form_select(array(
						"config" => array(
							"name" => "id_skpd",
							"class" => "form-control s2 id_skpd",
						),
						"list" => array("" => "") + $this->M_app->getSkpd(),
						"selected" => @$val->id_skpd,
					)),
				)
			);

			if ($this->session->userdata('id_level')!=1) {
				unset($data['form']['id_level']);
				unset($data['form']['id_skpd']);
			}

			$this->load->view("$this->module/form", $data);
		}

		public function profil() {
			$id = $this->session->userdata('id_admin');
			$this->form($id);
		}
		
		public function proses () {
			$this->output->unset_template();

			if ($this->input->post()) {
				$this->_rules();

				$back = "$this->module/form";
				$submsg = "Data gagal di proses";

				$this->_rules();

				if (!$this->form_validation->run()) {
					$submsg = $this->_formPostProsesError();
				} else {
					if (empty($this->input->post('id'))) {

						if ($this->_add_proses()) {
							$submsg = 'Data berhasil di proses';
							$back = "$this->module";
						}

					} elseif ($this->input->post('id') > 0) {

						if ($this->_edit_proses($this->input->post('id')))
							$submsg = "Data berhasil di proses"; $back = "$this->module";

					} else {
						show_404();
					}
					$this->_notif($back, $submsg);
				}
			} else {
				show_404();
			}
		}

		public function delete_proses () {
			$this->output->unset_template();
			if ($this->input->is_ajax_request() AND $this->input->post('id')) {
				$id = $this->input->post('id');
				$sql = $this->M_app->cekId($id);

				if ($sql->num_rows() > 0) {
					$val = $sql->row();

					if ($val->avatar != "" && file_exists(FCPATH . "$this->upload_path/$val->avatar")) {
						@unlink("{$this->upload_path}/thumb/$val->avatar");
						@unlink("{$this->upload_path}/$val->avatar");
					}

					$del = $this->M_app->delete($id);

					if ($del) {
						$this->stat = true;
					}
				}

				echo json_encode(array(
					"stat" => $this->stat
				));

			} else {
				show_404();
			}
		}

		public function _upload($file_element_name = "", $user_upload_path = "", $image = "") {
			$config['upload_path'] = $user_upload_path;
			$config['allowed_types'] = 'jpg|jpeg|png|bmp';
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload');
			$this->upload->initialize($config);

			$file_name = "";

			if ($this->upload->do_upload($file_element_name)){
				$data_upload = $this->upload->data();
				$file_name = $data_upload["file_name"];

				if ($file_element_name == 'avatar') {
					$config_resize['image_library'] = 'gd2';
					$config_resize['maintain_ratio'] = TRUE;
					$config_resize['master_dim'] = 'height';
					$config_resize['quality'] = "100%";
					$config_resize['source_image'] = $user_upload_path ."/". $file_name;
					$config_resize['new_image'] = "$user_upload_path/thumb/";
					$config_resize['width'] = 500;
					$config_resize['height'] = 500;

					$this->load->library('image_lib', $config_resize);

					if ( !$this->image_lib->resize() ) {
						$back = "$this->module";
						$submsg = "Resize File Failed";
						$this->stat = false;
						$this->_notif($back, $submsg);
					}

					if ($image != "") {
						@unlink("{$user_upload_path}/thumb/$image");
					}
				}

				if ($image != "") {
					@unlink("{$config['upload_path']}/$image");
				}
			} else {
				$back = "$this->module";
				$submsg = $this->upload->display_errors();
				$this->stat = false;
				$this->_notif($back, $submsg);
			}

			return $file_name;
		}

		public function check() {
			$this->output->unset_template();
			if($this->input->is_ajax_request()){
				if($this->input->post()){
					$username = $this->input->post('username');
					$id = $this->input->post('id');
					if ($username) {
						$check = $this->M_app->check_username($username, $id);
						if($check){
							echo "false";
						}else{
							echo "true";
						}
					}
				}
			}
		}

	// Private
		private function _add_proses () {
			$avatar = "";
			if ( !empty($_FILES['avatar']['name']) && isset($_FILES['avatar']['name']) ) {
				$file_element_name = 'avatar';
				$user_upload_path = $this->upload_path.'/';

				$upload = $this->_upload($file_element_name, $user_upload_path);
				$avatar = ($upload == "") ? $avatar : $upload;
			}

			$data = $this->_formPostInputData($avatar);
			$add = $this->M_app->add($data);

			if ($add) {
				$this->stat = true;
			}

			return $this->stat;
		}

		private function _edit_proses ($id) {
			$sql = $this->M_app->cekId($id);

			if ($sql->num_rows() > 0) {
				$val = $sql->row();

				$avatar = $val->avatar;

				if ( !empty($_FILES['avatar']['name']) && isset($_FILES['avatar']['name']) ) {
					$file_element_name = 'avatar';
					$user_upload_path = $this->upload_path.'/';

					$upload = $this->_upload($file_element_name, $user_upload_path, $avatar);
					$avatar = ($upload == "") ? $avatar : $upload;
				}

				$data = $this->_formPostInputData($avatar);
				$edit = $this->M_app->edit($data, $id);

				if ($edit) {
					$this->stat = true;
				}
			} else {
				show_404();
			}

			return $this->stat;
		}

		private function _formPostInputData ($avatar = "") {
			$username = $this->input->post("username", TRUE);
			$nama_lengkap = $this->input->post("nama_lengkap");
			$pass = $this->input->post("pass");
			$id_level = $this->input->post("id_level");
			$id_skpd = ($id_level != 2) ? "" : $this->input->post("id_skpd");

			$ret = array(
				"username" => $username,
				"nama_lengkap" => $nama_lengkap,
				"id_level" => $id_level,
				"id_skpd" => $id_skpd,
				"verification_code" => strtoupper(substr(md5($username.time()),2,7)),
				"ip_address" => $this->input->ip_address(),
				"avatar" => $avatar,
			);

			if (!empty($pass)) {
				$ret += array(
						"pass" => password_hash($pass, PASSWORD_BCRYPT, array("cost" => 12)),
					);
			}

			if ($username == $this->session->userdata('username')) {
				$new_sess = array(
					"nama_lengkap" => $nama_lengkap,
				);

				if ($avatar != "") {
					$image = "$avatar";
					$new_sess += array(
						"avatar" => $image,
					);
				}

				if ($id_level != "") {
					$level = $this->db
								->where('id_level', $id_level)
								->get('admin_level', 1)->row()->level;

					$new_sess += array(
						"level" => $level,
					);
				}

				$this->session->set_userdata($new_sess);
			}

			// jika admin skpd melakukan update
			if ($this->session->userdata('id_level') == 2) {
				unset($ret['id_level']);
				unset($ret['id_skpd']);
			}
			return $ret;
		}

		private function _formPostProsesError () {
			$err = "";

			if(form_error("nama_lengkap")) {
				$err .= form_error("nama_lengkap");
			}
			if(form_error("username")) {
				$err .= form_error("username");
			}
			if(form_error("pass")) {
				$err .= form_error("pass");
			}
			if(form_error("pass_confirm")) {
				$err .= form_error("pass_confirm");
			}
			if(form_error("id_level")) {
				$err .= form_error("id_level");
			}
			if(form_error("id_skpd")) {
				$err .= form_error("id_skpd");
			}


			return $err;
		}

		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			if ($this->session->userdata('id_level') == 1) {
				$config = array(
					array(
						"field" => "username",
						"label" => "Username",
						"rules" => "required",
						"errors" => array(
							"required" => "%s tidak boleh kosong"
						)
					),
					array(
						"field" => "nama_lengkap",
						"label" => "Nama Lengkap",
						"rules" => "required",
						"errors" => array(
							"required" => "%s tidak boleh kosong"
						)
					),
					array(
						"field" => "pass",
						"label" => "Password",
						"rules" => "trim",
					),
					array(
						"field" => "pass_confirm",
						"label" => "Konfirmasi Password",
						"rules" => "matches[pass]",
						"errors" => array(
							"matches" => "Konfirmasi Password tidak sama dengan Password"
						)
					),
					array(
						"field" => "id_level",
						"label" => "Level",
						"rules" => "required",
						"errors" => array(
							"required" => "%s tidak boleh kosong"
						)
					),
				);

				if ($this->input->post("id_level") == 2) {
					$config = array(
						array(
							"field" => "id_skpd",
							"label" => "SKPD",
							"rules" => "required",
							"errors" => array(
								"required" => "%s tidak boleh kosong"
							)
						),
						);
					}
			}
			else {
				$config = array(
					array(
						"field" => "username",
						"label" => "Username",
						"rules" => "required",
						"errors" => array(
							"required" => "%s tidak boleh kosong"
						)
					),
					array(
						"field" => "nama_lengkap",
						"label" => "Nama Lengkap",
						"rules" => "required",
						"errors" => array(
							"required" => "%s tidak boleh kosong"
						)
					),
					array(
						"field" => "pass",
						"label" => "Password",
						"rules" => "trim",
					),
					array(
						"field" => "pass_confirm",
						"label" => "Konfirmasi Password",
						"rules" => "matches[pass]",
						"errors" => array(
							"matches" => "Konfirmasi Password tidak sama dengan Password"
						)
					),
				);
			}
			$this->form_validation->set_error_delimiters("<div class='' style='color: red;'>", "</div>");
			$this->form_validation->set_rules($config);
		}

		private function _notif ($back, $submsg = "") {
			if ($this->stat) {
				$this->session->set_flashdata( "message", '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>Berhasil <br/><span>'.$submsg.'</div>' );
			} else {
				$this->session->set_flashdata( "message", '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>Gagal <br/><span>'.$submsg.'<br />'. $this->session->flashdata('errors') .'</div>' );
			}

			redirect($back);
		}

}

/* End of file Admin.php */
/* Location: ./application/modules/admin/controllers/Admin.php */
